import React from 'react'
import { AnyStyledComponent } from 'styled-components'
import {
  HorizontalEndResizerButtonInner,
  HorizontalStartResizerButtonInner,
  ResizerButtonInnerProps,
  VerticalEndResizerButtonInner,
  VerticalStartResizerButtonInner,
} from './ResizerButtonInner'
import { ResizerDirection, ResizerSide } from './types'

type Inners = {
  [direction in ResizerDirection]: { [side in ResizerSide]: AnyStyledComponent }
}

const inners: Inners = {
  column: {
    end: VerticalEndResizerButtonInner,
    start: VerticalStartResizerButtonInner,
  },
  row: {
    end: HorizontalEndResizerButtonInner,
    start: HorizontalStartResizerButtonInner,
  },
}

interface Props extends React.HTMLProps<HTMLButtonElement> {
  direction: ResizerDirection
  isCollapsed: boolean
  isVisible: boolean
  side: ResizerSide
  buttonInner?: React.ComponentType<ResizerButtonInnerProps>
}

export class ResizerButton extends React.PureComponent<Props> {
  public static defaultProps = {
    isCollapsed: false,
    isVisible: false,
  }

  public render() {
    const {
      buttonInner,
      direction,
      side,
      isCollapsed,
      onClick,
      isVisible,
    } = this.props

    const ResizerButtonInner = buttonInner || inners[direction][side]

    return (
      <ResizerButtonInner
        aria-expanded={!isCollapsed}
        isCollapsed={isCollapsed}
        isVisible={isVisible}
        onClick={onClick}
        onMouseDown={(event: MouseEvent) => event.preventDefault()}
      />
    )
  }
}
